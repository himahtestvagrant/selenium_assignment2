package remote;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class RemoteLoad {
    public static WebDriver driver;
    public static void main(String[] args) throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("browserName","chrome");


        //create object of RemoteWebDriver
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),desiredCapabilities);


        // launch URL
        driver.get("https://www.google.com");

    }
}
